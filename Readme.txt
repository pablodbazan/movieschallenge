1. ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?

El principio de responsabilidad única es parte de los principios SOLID que representan los principios básicos de la programación orientada a objetos. 
Establece que cada clase debe tener responsabilidad sobre una sola parte del sistema y que esta funcionalidad debe estar encapsulada en su totalidad por esta clase.
El propósito de esto es que una clase debería tener una sola razópn para cambiar y la responsabilidad de dicha clase es el eje dcel cambio.

2. ¿Qué características tiene, según su opinión, un “buen” código o código limpio?

Un buen código o código limpio debe ser fácil de leer y entender, debe ser escalable y mantenible. Debe contener comentarios sólo en los casos en que el código no sea lo suficientemente claro respecto de la función que cumple.
Al crear funciones, estas deben ser, en lo posible, simples, claras y pequeñas y deben tener nombres significativos. No debe haber cóodigo repetido, es decir que hay que evitar que dos partes del programa que realicen la misma función.
