package com.pablobazan.movieschallenge;

import com.pablobazan.movieschallenge.entities.Genre;
import com.pablobazan.movieschallenge.entities.ImagesConfiguration;
import com.pablobazan.movieschallenge.entities.Movie;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MovieUnitTest {
    String movieJson = "{" +
                "\"adult\": false," +
                "\"backdrop_path\": \"/2RSirqZG949GuRwN38MYCIGG4Od.jpg\"," +
                "\"genre_ids\": [" +
                    "27," +
                    "53" +
                "]," +
                "\"id\": 985939," +
                "\"original_language\": \"en\"," +
                "\"original_title\": \"Fall\"," +
                "\"overview\": \"Para las mejores amigas Becky y Hunter, la vida trata de superar tus miedos y empujar tus límites. Sin embargo, después de subir hasta la cima de una torre de comunicaciones abandonada, se encuentran atrapadas y sin forma de bajar. A 600 metros del suelo y totalmente alejadas de la civilización, las chicas pondrán a prueba sus habilidades de escaladoras expertas y lucharán desesperadamente por sobrevivir aunque lo tengan todo en contra. ¿Lo conseguirán?\"," +
                "\"popularity\": 8806.711," +
                "\"poster_path\": \"/cOWPC7Sg9xoDM5i3uzs4iQtSPUj.jpg\"," +
                "\"release_date\": \"2022-08-11\"," +
                "\"title\": \"Vértigo\"," +
                "\"video\": false," +
                "\"vote_average\": 7.5," +
                "\"vote_count\": 951" +
            "}";

    String movieListJson = "{" +
            "\"results\": [" +
                "{" +
                    "\"adult\": false," +
                    "\"backdrop_path\": \"/2RSirqZG949GuRwN38MYCIGG4Od.jpg\"," +
                    "\"genre_ids\": [" +
                        "53" +
                    "]," +
                    "\"id\": 985939," +
                    "\"original_language\": \"en\"," +
                    "\"original_title\": \"Fall\"," +
                    "\"overview\": \"Para las mejores amigas Becky y Hunter, la vida trata de superar tus miedos y empujar tus límites. Sin embargo, después de subir hasta la cima de una torre de comunicaciones abandonada, se encuentran atrapadas y sin forma de bajar. A 600 metros del suelo y totalmente alejadas de la civilización, las chicas pondrán a prueba sus habilidades de escaladoras expertas y lucharán desesperadamente por sobrevivir aunque lo tengan todo en contra. ¿Lo conseguirán?\"," +
                    "\"popularity\": 8806.711," +
                    "\"poster_path\": \"/cOWPC7Sg9xoDM5i3uzs4iQtSPUj.jpg\"," +
                    "\"release_date\": \"2022-08-11\"," +
                    "\"title\": \"Vértigo\"," +
                    "\"video\": false," +
                    "\"vote_average\": 7.5," +
                    "\"vote_count\": 951" +
                "}," +
                "{" +
                    "\"adult\": false," +
                    "\"backdrop_path\": \"/5GA3vV1aWWHTSDO5eno8V5zDo8r.jpg\"," +
                    "\"genre_ids\": [" +
                        "27," +
                        "53" +
                    "]," +
                    "\"id\": 760161," +
                    "\"original_language\": \"en\"," +
                    "\"original_title\": \"Orphan: First Kill\"," +
                    "\"overview\": \"Tras escapar de un centro psiquiátrico estonio, Leena Klammer viaja a América haciéndose pasar por Esther, la hija desaparecida de una familia adinerada. Pero cuando su máscara empieza a caer, se enfrenta a una madre que protegerá a su familia de la \\\"niña\\\" asesina a cualquier precio.\"," +
                    "\"popularity\": 6280.62," +
                    "\"poster_path\": \"/nODIZa8p2Y31CKlp5JS0LFRmeXF.jpg\"," +
                    "\"release_date\": \"2022-07-27\"," +
                    "\"title\": \"La Huérfana: El Origen\"," +
                    "\"video\": false," +
                    "\"vote_average\": 7," +
                    "\"vote_count\": 703" +
                "}" +
            "]" +
        "}";

    String genreListJson = "{\n" +
            "\"genres\": [\n" +
            "{\n" +
            "\"id\": 27,\n" +
            "\"name\": \"Action\"\n" +
            "},\n" +
            "{\n" +
            "\"id\": 53,\n" +
            "\"name\": \"Adventure\"\n" +
            "}\n" +
            "]\n" +
            "}";

    @Before
    public void init(){
    }

    @Test
    public void newMovie_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        assertMovie(movie, json);
    }

    private void assertMovie(Movie movie, JSONObject json)throws Exception {
        assertEquals(movie.adult,json.getBoolean("adult"));
        assertEquals(movie.backdropPath,json.getString("backdrop_path"));
        assertEquals(movie.id,json.getLong("id"));
        assertEquals(movie.originalLanguage,json.getString("original_language"));
        assertEquals(movie.originalTitle,json.getString("original_title"));
        assertEquals(movie.overview,json.getString("overview"));
        assertEquals(movie.popularity,json.getDouble("popularity"), 0);
        assertEquals(movie.posterPath,json.getString("poster_path"));
        assertEquals(movie.releaseDate,json.getString("release_date"));
        assertEquals(movie.title,json.getString("title"));
        assertEquals(movie.video,json.getBoolean("video"));
        assertEquals(movie.voteAverage,json.getDouble("vote_average"), 0);
        assertEquals(movie.voteCount,json.getLong("vote_count"));
        assertEqualsArray(movie.genreIds, json.getJSONArray("genre_ids"));
    }

    private void assertEqualsArray(List<Long>list, JSONArray jsonArray)throws Exception{
        assertEquals(list.size(),jsonArray.length());
        for(int i = 0; i<jsonArray.length() ; i++){
            assertEquals((long)list.get(i),jsonArray.getLong(i));
        }
    }

    private void assertEqualsMovieList(List<Movie>list, JSONArray jsonArray)throws Exception{
        assertEquals(list.size(),jsonArray.length());
        for(int i = 0; i<jsonArray.length() ; i++){
            assertMovie(list.get(i), jsonArray.getJSONObject(i));
        }
    }

    @Test(expected= Exception.class)
    public void newMovie_jsonIsNull() throws Exception {
        new Movie(null);
    }

    @Test(expected= Exception.class)
    public void newMovie_wrongTypes() throws Exception {
        JSONObject json = new JSONObject();
        json.put("video", "1");
        new Movie(json);
    }

    @Test
    public void ParseMovieList_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieListJson);
        List<Movie> list = Movie.ParseMovieList(json);
        assertEqualsMovieList(list, json.getJSONArray("results"));
    }

    @Test
    public void ParseMovieList_wrongArrayName() throws Exception {
        String listJson = "{" +
                    "\"movies\": [" +
                    "]" +
                "}";
        JSONObject json = new JSONObject(listJson);
        List<Movie> list = Movie.ParseMovieList(json);
        assertEquals(list,null);
    }

    @Test
    public void ParseMovieList_emptyArray() throws Exception {
        String listJson = "{" +
                "\"results\": [" +
                "]" +
            "}";
        JSONObject json = new JSONObject(listJson);
        List<Movie> list = Movie.ParseMovieList(json);
        assertEquals(list.size(),0);
    }

    @Test(expected= Exception.class)
    public void ParseMovieList_nullInput() throws Exception {
        Movie.ParseMovieList(null);
    }

    @Test
    public void getBackdropSmallSizePath_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.backdropSizes = new ArrayList<>();
        config.backdropSizes.add("small");
        config.backdropSizes.add("original");
        String value = movie.getBackdropSmallSizePath(config);
        assertEquals(value,config.secureBaseUrl+config.backdropSizes.get(0)+movie.backdropPath);
    }

    @Test
    public void getBackdropSmallSizePath_nullConfig() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        String value = movie.getBackdropSmallSizePath(null);
        assertEquals(value,"");
    }

    @Test
    public void getBackdropSmallSizePath_nullArray() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        String value = movie.getBackdropSmallSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getBackdropSmallSizePath_nullPosterPath() throws Exception {
        Movie movie = new Movie();
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.backdropSizes = new ArrayList<>();
        config.backdropSizes.add("small");
        config.backdropSizes.add("original");
        String value = movie.getBackdropSmallSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getBackdropOriginalSizePath_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.backdropSizes = new ArrayList<>();
        config.backdropSizes.add("small");
        config.backdropSizes.add("original");
        String value = movie.getBackdropOriginalSizePath(config);
        assertEquals(value,config.secureBaseUrl+config.backdropSizes.get(config.backdropSizes.size()-1)+movie.backdropPath);
    }

    @Test
    public void getBackdropOriginalSizePath_nullConfig() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        String value = movie.getBackdropOriginalSizePath(null);
        assertEquals(value,"");
    }

    @Test
    public void getBackdropOriginalSizePath_nullArray() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        String value = movie.getBackdropOriginalSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getBackdropOriginalSizePath_nullPosterPath() throws Exception {
        Movie movie = new Movie();
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.backdropSizes = new ArrayList<>();
        config.backdropSizes.add("small");
        config.backdropSizes.add("original");
        String value = movie.getBackdropOriginalSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getPosterSmallSizePath_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.posterSizes = new ArrayList<>();
        config.posterSizes.add("small");
        config.posterSizes.add("original");
        String value = movie.getPosterSmallSizePath(config);
        assertEquals(value,config.secureBaseUrl+config.posterSizes.get(0)+movie.posterPath);
    }

    @Test
    public void getPosterSmallSizePath_nullConfig() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        String value = movie.getPosterSmallSizePath(null);
        assertEquals(value,"");
    }

    @Test
    public void getPosterSmallSizePath_nullArray() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        String value = movie.getPosterSmallSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getPosterSmallSizePath_nullPosterPath() throws Exception {
        Movie movie = new Movie();
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.posterSizes = new ArrayList<>();
        config.posterSizes.add("small");
        config.posterSizes.add("original");
        String value = movie.getPosterSmallSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getPosterOriginalSizePath_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.posterSizes = new ArrayList<>();
        config.posterSizes.add("small");
        config.posterSizes.add("original");
        String value = movie.getPosterOriginalSizePath(config);
        assertEquals(value,config.secureBaseUrl+config.posterSizes.get(config.posterSizes.size()-1)+movie.posterPath);
    }

    @Test
    public void getPosterOriginalSizePath_nullConfig() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        String value = movie.getPosterOriginalSizePath(null);
        assertEquals(value,"");
    }

    @Test
    public void getPosterOriginalSizePath_nullArray() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        String value = movie.getPosterOriginalSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getPosterOriginalSizePath_nullPosterPath() throws Exception {
        Movie movie = new Movie();
        ImagesConfiguration config = new ImagesConfiguration();
        config.secureBaseUrl = "test.org/";
        config.posterSizes = new ArrayList<>();
        config.posterSizes.add("small");
        config.posterSizes.add("original");
        String value = movie.getPosterOriginalSizePath(config);
        assertEquals(value,"");
    }

    @Test
    public void getGenreDescriptions_isCorrect() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        JSONObject genreJson = new JSONObject(genreListJson);
        Movie movie = new Movie(json);
        List<Genre> genres = Genre.ParseGenreList(genreJson);
        String value = movie.getGenreDescriptions(genres);
        assertEquals(value, "Action-Adventure");
    }

    @Test
    public void getGenreDescriptions_nullImput() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        String value = movie.getGenreDescriptions(null);
        assertEquals(value, "");
    }

    @Test
    public void getGenreDescriptions_emptyList() throws Exception {
        JSONObject json = new JSONObject(movieJson);
        Movie movie = new Movie(json);
        List<Genre> genres = new ArrayList<>();
        String value = movie.getGenreDescriptions(genres);
        assertEquals(value, "");
    }

    @Test
    public void getYear_isCorrect() throws Exception {
        Movie movie = new Movie();
        movie.releaseDate = "2022-08-11";
        String value = movie.getYear();
        assertEquals(value, "2022");
    }

    @Test
    public void getYear_nullDate() throws Exception {
        Movie movie = new Movie();
        String value = movie.getYear();
        assertEquals(value, "");
    }

    @Test
    public void getYear_wrongFotmat() throws Exception {
        Movie movie = new Movie();
        movie.releaseDate = "test";
        String value = movie.getYear();
        assertEquals(value, "");
    }
}