package com.pablobazan.movieschallenge;

import android.util.JsonReader;

import com.pablobazan.movieschallenge.entities.Genre;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class GenreUnitTest {
    @Before
    public void init(){
    }

    @Test
    public void newGenre_isCorrect() throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", 28);
        json.put("name", "Action");
        Genre genre = new Genre(json);
        assertEquals(genre.id,28);
        assertEquals(genre.name,"Action");
    }

    @Test(expected= Exception.class)
    public void newGenre_jsonIsNull() throws Exception {
        new Genre(null);
    }

    @Test(expected= Exception.class)
    public void newGenre_wrongTypes() throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", "28");
        json.put("name", 1);
        new Genre(json);
    }

    @Test
    public void ParseGenreList_isCorrect() throws Exception {
        String genreListJson = "{\n" +
                                "\"genres\": [\n" +
                                    "{\n" +
                                        "\"id\": 28,\n" +
                                        "\"name\": \"Action\"\n" +
                                    "},\n" +
                                    "{\n" +
                                        "\"id\": 12,\n" +
                                        "\"name\": \"Adventure\"\n" +
                                    "}\n" +
                                "]\n" +
                            "}";
        JSONObject json = new JSONObject(genreListJson);
        List<Genre> genreList = Genre.ParseGenreList(json);
        assertEquals(genreList.size(),2);
        assertEquals(genreList.get(0).id,28);
        assertEquals(genreList.get(0).name,"Action");
        assertEquals(genreList.get(1).id,12);
        assertEquals(genreList.get(1).name,"Adventure");
    }

    @Test
    public void ParseGenreList_wrongArrayName() throws Exception {
        String genreListJson = "{\n" +
                "\"genre\": [\n" +
                "{\n" +
                "\"id\": 28,\n" +
                "\"name\": \"Action\"\n" +
                "},\n" +
                "{\n" +
                "\"id\": 12,\n" +
                "\"name\": \"Adventure\"\n" +
                "}\n" +
                "]\n" +
                "}";
        JSONObject json = new JSONObject(genreListJson);
        List<Genre> genreList = Genre.ParseGenreList(json);
        assertEquals(genreList,null);
    }

    @Test
    public void ParseGenreList_emptyArray() throws Exception {
        String genreListJson = "{\n" +
                "\"genres\": [\n" +
                "]\n" +
            "}";
        JSONObject json = new JSONObject(genreListJson);
        List<Genre> genreList = Genre.ParseGenreList(json);
        assertEquals(genreList.size(),0);
    }

    @Test(expected= Exception.class)
    public void ParseGenreList_nullInput() throws Exception {
        Genre.ParseGenreList(null);
    }
}