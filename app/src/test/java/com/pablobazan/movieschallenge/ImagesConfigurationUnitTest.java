package com.pablobazan.movieschallenge;

import com.google.gson.JsonArray;
import com.pablobazan.movieschallenge.entities.Genre;
import com.pablobazan.movieschallenge.entities.ImagesConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ImagesConfigurationUnitTest {
    String imagesConfigJson = "{\"images\": {" +
                "\"base_url\": \"http://image.tmdb.org/t/p/\"," +
                "\"secure_base_url\": \"https://image.tmdb.org/t/p/\"," +
                "\"backdrop_sizes\": [" +
                    "\"w300\"," +
                    "\"w780\"," +
                    "\"w1280\"," +
                    "\"original\"" +
                "]," +
                "\"logo_sizes\": [" +
                    "\"w45\"," +
                    "\"w92\"," +
                    "\"w154\"," +
                    "\"w185\"," +
                    "\"w300\"," +
                    "\"w500\"," +
                    "\"original\"" +
                "]," +
                "\"poster_sizes\": [" +
                    "\"w92\"," +
                    "\"w154\"," +
                    "\"w185\"," +
                    "\"w342\"," +
                    "\"w500\"," +
                    "\"w780\"," +
                    "\"original\"" +
                "]," +
                "\"profile_sizes\": [" +
                    "\"w45\"," +
                    "\"w185\"," +
                    "\"h632\"," +
                    "\"original\"" +
                "]," +
                "\"still_sizes\": [" +
                    "\"w92\"," +
                    "\"w185\"," +
                    "\"w300\"," +
                    "\"original\"" +
                "]" +
            "}},";

    @Before
    public void init(){
    }

    @Test
    public void newImagesConfiguration_isCorrect() throws Exception {
        JSONObject json = new JSONObject(imagesConfigJson);
        ImagesConfiguration config = new ImagesConfiguration(json);
        JSONObject images = json.getJSONObject("images");
        assertEquals(config.baseUrl,images.getString("base_url"));
        assertEquals(config.secureBaseUrl,images.getString("secure_base_url"));
        assertEqualsArray(config.backdropSizes, images.getJSONArray("backdrop_sizes"));
        assertEqualsArray(config.logoSizes, images.getJSONArray("logo_sizes"));
        assertEqualsArray(config.posterSizes, images.getJSONArray("poster_sizes"));
        assertEqualsArray(config.profileSizes, images.getJSONArray("profile_sizes"));
        assertEqualsArray(config.stillSizes, images.getJSONArray("still_sizes"));
    }

    private void assertEqualsArray(List<String>list, JSONArray jsonArray)throws Exception{
        assertEquals(list.size(),jsonArray.length());
        for(int i = 0; i<jsonArray.length() ; i++){
            assertEquals(list.get(i),jsonArray.getString(i));
        }
    }

    @Test(expected= Exception.class)
    public void newImagesConfiguration_jsonIsNull() throws Exception {
        new ImagesConfiguration(null);
    }
}