package com.pablobazan.movieschallenge.services;
import android.content.Context;

import com.pablobazan.movieschallenge.R;
import com.pablobazan.movieschallenge.entities.Genre;
import com.pablobazan.movieschallenge.entities.ImagesConfiguration;
import com.pablobazan.movieschallenge.entities.Movie;
import com.pablobazan.movieschallenge.interfaces.OnConfigurationReceivedListener;
import com.pablobazan.movieschallenge.interfaces.OnErrorReceived;
import com.pablobazan.movieschallenge.interfaces.OnGenreListReceived;
import com.pablobazan.movieschallenge.interfaces.OnMovieListReceivedListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MovieService extends BaseCommunicationService{
    private static final String baseUrl = "https://api.themoviedb.org";
    private static final String configurationUrl = baseUrl + "/3/configuration";
    private static final String upcomingUrl = baseUrl + "/3/movie/upcoming";
    private static final String topRatedUrl = baseUrl + "/3/movie/top_rated";
    private static final String popularUrl = baseUrl + "/3/movie/popular";
    private static final String genreUrl = baseUrl + "/3/genre/movie/list";

    private static final String apiKey = "923599df2fff7ce4aedee1a373350dfc";

    public MovieService() { }

    public void getConfiguration(Context context, OnConfigurationReceivedListener listener, OnErrorReceived errorListener){
        if(checkConnection(context, errorListener)){
            executeRemoteCall(context, configurationUrl+"?api_key="+apiKey,GET,null,errorListener, json -> {
                try{
                    ImagesConfiguration config = new ImagesConfiguration(json);
                    if(listener != null)
                        listener.OnConfigurationReceived(config);
                }
                catch (Exception ex){
                    errorListener.OnError(context.getString(R.string.communication_service_error_data_error));
                }
            });
        }
    }

    public void getGenreList(Context context, OnGenreListReceived listener, OnErrorReceived errorListener){
        if(checkConnection(context, errorListener)){
            executeRemoteCall(context, genreUrl+"?api_key="+apiKey,GET,null,errorListener, json -> {
                try{
                    List<Genre> list = Genre.ParseGenreList(json);
                    if(listener != null)
                        listener.OnGenreReceived(list);
                }
                catch (Exception ex){
                    errorListener.OnError(context.getString(R.string.communication_service_error_data_error));
                }
            });
        }
    }

    public void getUpcomingMovies(Context context, int page, OnMovieListReceivedListener listener, OnErrorReceived errorListener){
        if(checkConnection(context, errorListener)){
            String url =upcomingUrl+"?api_key="+apiKey+"&page="+page;
            String language = getLanguageQuery();
            if(language != null && !language.equals("")){
                url += language;
            }
            executeRemoteCall(context, url,GET,null,errorListener, json -> {
                try{
                    List<Movie> list = Movie.ParseMovieList(json);
                    if(listener != null)
                        listener.OnMovieListReceived(list);
                }
                catch (Exception ex){
                    errorListener.OnError(context.getString(R.string.communication_service_error_data_error));
                }
            });
        }
    }

    public void getTopRatedMovies(Context context, int page, OnMovieListReceivedListener listener, OnErrorReceived errorListener){
        if(checkConnection(context, errorListener)){
            String url =topRatedUrl+"?api_key="+apiKey+"&page="+page;
            String language = getLanguageQuery();
            if(language != null && !language.equals("")){
                url += language;
            }
            executeRemoteCall(context, url,GET,null,errorListener, json -> {
                try{
                    List<Movie> list = Movie.ParseMovieList(json);
                    if(listener != null)
                        listener.OnMovieListReceived(list);
                }
                catch (Exception ex){
                    errorListener.OnError(context.getString(R.string.communication_service_error_data_error));
                }
            });
        }
    }

    public void getPopularMovies(Context context, int page, OnMovieListReceivedListener listener, OnErrorReceived errorListener){
        if(checkConnection(context, errorListener)){
            String url =popularUrl+"?api_key="+apiKey+"&page="+page;
            String language = getLanguageQuery();
            if(language != null && !language.equals("")){
                url += language;
            }
            executeRemoteCall(context, url, GET,null,errorListener, json -> {
                try{
                    List<Movie> list = Movie.ParseMovieList(json);
                    if(listener != null)
                        listener.OnMovieListReceived(list);
                }
                catch (Exception ex){
                    errorListener.OnError(context.getString(R.string.communication_service_error_data_error));
                }
            });
        }
    }

    private String getLanguageQuery(){
        Locale locale = Locale.getDefault();
        if(locale != null){
            return "&language=" + locale.getLanguage() + "-" + locale.getCountry();
        }
        return "";
    }
}
