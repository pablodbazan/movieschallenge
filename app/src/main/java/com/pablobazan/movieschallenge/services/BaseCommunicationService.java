package com.pablobazan.movieschallenge.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pablobazan.movieschallenge.R;
import com.pablobazan.movieschallenge.interfaces.OnErrorReceived;

import org.json.JSONObject;

import java.util.HashMap;

public class BaseCommunicationService {
    protected static final int POST = Request.Method.POST;
    protected static final int GET = Request.Method.GET;
    protected static final int PUT = Request.Method.PUT;
    protected static final int DELETE = Request.Method.DELETE;
    protected static final int HEAD = Request.Method.HEAD;
    protected static final int OPTIONS = Request.Method.OPTIONS;
    protected static final int TRACE = Request.Method.TRACE;
    protected static final int PATCH = Request.Method.PATCH;

    protected boolean checkConnection(Context context, OnErrorReceived listener){
        ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        boolean isConnected = netInfo != null && netInfo.isConnected();
        if(!isConnected && listener != null){
            listener.OnError(context.getString(R.string.communication_service_error_no_connection));
        }
        return isConnected;
    }

    protected void executeRemoteCall(Context context, String url, int method, HashMap<String, Object> params, OnErrorReceived errorListener, Response.Listener<JSONObject> listener){
        JSONObject json = new JSONObject();
        if(params != null)
            json = new JSONObject(params);
        JsonObjectRequest jsonObject = new JsonObjectRequest(method, url, json, listener, error -> processErrorResponse(context, error, errorListener));

        RequestQueue queue = Volley.newRequestQueue(context);
        //jsonObject.setRetryPolicy(new DefaultRetryPolicy(COMM_RETRIES , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObject);
    }

    private void processErrorResponse(Context context, VolleyError error, OnErrorReceived listener){
        String errorMessage = context.getResources().getString(R.string.communication_service_error_unknown) + ": " + error.getMessage();
        boolean logout = false;
        try{
            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                //This indicates that the request has either time out or there is no connection
                errorMessage = context.getResources().getString(R.string.communication_service_error_no_connection) + ": " + error.getMessage();
            } else if (error instanceof AuthFailureError) {
                // Error indicating that there was an Authentication Failure while performing the request
                errorMessage = context.getResources().getString(R.string.communication_service_error_auth_failure) + ": " + error.getMessage();
            } else if (error instanceof ServerError) {
                //Indicates that the server responded with a error response
                errorMessage = context.getResources().getString(R.string.communication_service_error) + ": " + error.getMessage();
            } else if (error instanceof NetworkError) {
                //Indicates that there was network error while performing the request
                errorMessage = context.getResources().getString(R.string.communication_service_error_network_error) + ": " + error.getMessage();
            } else if (error instanceof ParseError) {
                // Indicates that the server response could not be parsed
                errorMessage = context.getResources().getString(R.string.communication_service_error_parser_error) + ": " + error.getMessage();
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        if(listener != null){
            listener.OnError(errorMessage);
        }
    }
}
