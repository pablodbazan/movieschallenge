package com.pablobazan.movieschallenge.interfaces;

import com.pablobazan.movieschallenge.entities.Movie;

import java.util.List;

public interface OnErrorReceived {
    void OnError(String error);
}
