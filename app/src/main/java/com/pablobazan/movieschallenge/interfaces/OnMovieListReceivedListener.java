package com.pablobazan.movieschallenge.interfaces;

import com.pablobazan.movieschallenge.entities.Movie;

import java.util.List;

public interface OnMovieListReceivedListener {
    void OnMovieListReceived(List<Movie> movieList);
}
