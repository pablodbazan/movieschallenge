package com.pablobazan.movieschallenge.interfaces;

import com.pablobazan.movieschallenge.entities.Genre;

import java.util.List;

public interface OnGenreListReceived {
    void OnGenreReceived(List<Genre> genreList);
}
