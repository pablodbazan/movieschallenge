package com.pablobazan.movieschallenge.interfaces;

import com.pablobazan.movieschallenge.entities.Movie;

public interface OnMovieClickedListener {
    void OnMovieClicked(Movie movie);
}
