package com.pablobazan.movieschallenge.interfaces;
import com.pablobazan.movieschallenge.entities.ImagesConfiguration;

import java.util.List;

public interface OnConfigurationReceivedListener {
    void OnConfigurationReceived(ImagesConfiguration imagesConfiguration);
}
