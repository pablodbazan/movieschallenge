package com.pablobazan.movieschallenge.entities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ImagesConfiguration implements Serializable {
    public String baseUrl;
    public String secureBaseUrl;
    public List<String> backdropSizes;
    public List<String> logoSizes;
    public List<String> posterSizes;
    public List<String> profileSizes;
    public List<String> stillSizes;

    public ImagesConfiguration() {}

    public ImagesConfiguration(JSONObject json) throws  Exception{
        if(json.has("images") && !json.isNull("images")){
            JSONObject images = json.getJSONObject("images");
            if(images.has("base_url") && !images.isNull("base_url")){
                this.baseUrl = images.getString("base_url");
            }
            if(images.has("secure_base_url") && !images.isNull("secure_base_url")){
                this.secureBaseUrl = images.getString("secure_base_url");
            }
            if(images.has("backdrop_sizes") && !images.isNull("backdrop_sizes")){
                this.backdropSizes = new ArrayList<>();
                JSONArray array = images.getJSONArray("backdrop_sizes");
                for(int i=0; i<array.length(); i++){
                    this.backdropSizes.add(array.getString(i));
                }
            }
            if(images.has("logo_sizes") && !images.isNull("logo_sizes")){
                this.logoSizes = new ArrayList<>();
                JSONArray array = images.getJSONArray("logo_sizes");
                for(int i=0; i<array.length(); i++){
                    this.logoSizes.add(array.getString(i));
                }
            }
            if(images.has("poster_sizes") && !images.isNull("poster_sizes")){
                this.posterSizes = new ArrayList<>();
                JSONArray array = images.getJSONArray("poster_sizes");
                for(int i=0; i<array.length(); i++){
                    this.posterSizes.add(array.getString(i));
                }
            }
            if(images.has("profile_sizes") && !images.isNull("profile_sizes")){
                this.profileSizes = new ArrayList<>();
                JSONArray array = images.getJSONArray("profile_sizes");
                for(int i=0; i<array.length(); i++){
                    this.profileSizes.add(array.getString(i));
                }
            }
            if(images.has("still_sizes") && !images.isNull("still_sizes")){
                this.stillSizes = new ArrayList<>();
                JSONArray array = images.getJSONArray("still_sizes");
                for(int i=0; i<array.length(); i++){
                    this.stillSizes.add(array.getString(i));
                }
            }
        }
    }
}
