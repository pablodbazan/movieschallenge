package com.pablobazan.movieschallenge.entities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Genre implements Serializable {
    public long id;
    public String name;

    public Genre(int id, String name){
        this.id = id;
        this.name = name;
    }

    public Genre(JSONObject json) throws Exception{
        if(json.has("id") && !json.isNull("id")){
            this.id = json.getLong("id");
        }
        if(json.has("name") && !json.isNull("name")){
            this.name = json.getString("name");
        }
    }

    public static List<Genre> ParseGenreList(JSONObject json)throws Exception{
        List<Genre> list = null;
        if(json.has("genres") && !json.isNull("genres")){
            list = new ArrayList<>();
            JSONArray array = json.getJSONArray("genres");
            for(int i=0; i<array.length(); i++){
                list.add(new Genre(array.getJSONObject(i)));
            }
        }
        return list;
    }
}
