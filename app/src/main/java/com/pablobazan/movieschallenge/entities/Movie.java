package com.pablobazan.movieschallenge.entities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Movie  implements Serializable {
    public long id;

    public boolean adult;
    public String backdropPath;
    public List<Long> genreIds;
    public double popularity;
    public String title;
    public String originalTitle;
    public String posterPath;
    public String originalLanguage;
    public String overview;
    public String releaseDate;
    public boolean video;
    public double voteAverage;
    public long voteCount;

    public Movie(){}

    public Movie(long id, String title, String imageUrl, String genre, String stars, String language, String year, String plot, String trailerUrl){
        this.id = id;
        this.title = title;
        this.posterPath = imageUrl;
        this.originalLanguage = language;
        this.releaseDate = year;
        this.overview = plot;
    }

    public Movie (JSONObject json)throws Exception{
        if(json.has("adult") && !json.isNull("adult")){
            this.adult = json.getBoolean("adult");
        }
        if(json.has("backdrop_path") && !json.isNull("backdrop_path")){
            this.backdropPath = json.getString("backdrop_path");
        }
        if(json.has("genre_ids") && !json.isNull("genre_ids")){
            this.genreIds = new ArrayList<>();
            JSONArray array = json.getJSONArray("genre_ids");
            for(int i=0; i<array.length(); i++){
                this.genreIds.add(array.getLong(i));
            }
        }
        if(json.has("id") && !json.isNull("id")){
            this.id = json.getLong("id");
        }
        if(json.has("original_language") && !json.isNull("original_language")){
            this.originalLanguage = json.getString("original_language");
        }
        if(json.has("original_title") && !json.isNull("original_title")){
            this.originalTitle = json.getString("original_title");
        }
        if(json.has("overview") && !json.isNull("overview")){
            this.overview = json.getString("overview");
        }
        if(json.has("popularity") && !json.isNull("popularity")){
            this.popularity = json.getDouble("popularity");
        }
        if(json.has("poster_path") && !json.isNull("poster_path")){
            this.posterPath = json.getString("poster_path");
        }
        if(json.has("release_date") && !json.isNull("release_date")){
            this.releaseDate = json.getString("release_date");
        }
        if(json.has("title") && !json.isNull("title")){
            this.title = json.getString("title");
        }
        if(json.has("video") && !json.isNull("video")){
            this.video = json.getBoolean("video");
        }
        if(json.has("vote_average") && !json.isNull("vote_average")){
            this.voteAverage = json.getDouble("vote_average");
        }
        if(json.has("vote_count") && !json.isNull("vote_count")){
            this.voteCount = json.getLong("vote_count");
        }
    }

    public static List<Movie> ParseMovieList(JSONObject json) throws Exception{
        List<Movie> list = null;
        if(json.has("results") && !json.isNull("results")){
            list = new ArrayList<>();
            JSONArray array = json.getJSONArray("results");
            for(int i=0; i<array.length(); i++){
                list.add(new Movie(array.getJSONObject(i)));
            }
        }
        return list;
    }

    public String getBackdropSmallSizePath(ImagesConfiguration config){
        if(this.backdropPath!=null && config != null && config.backdropSizes != null && config.backdropSizes.size()>0){
            return config.secureBaseUrl + config.backdropSizes.get(0) + this.backdropPath;
        }
        return "";
    }

    public String getBackdropOriginalSizePath(ImagesConfiguration config){
        if(this.backdropPath!=null && config != null && config.backdropSizes != null && config.backdropSizes.size()>0){
            return config.secureBaseUrl + config.backdropSizes.get(config.backdropSizes.size()-1) + this.backdropPath;
        }
        return "";
    }

    public String getPosterSmallSizePath(ImagesConfiguration config){
        if(this.posterPath!=null && config != null && config.posterSizes != null && config.posterSizes.size()>0){
            return config.secureBaseUrl + config.posterSizes.get(0) + this.posterPath;
        }
        return "";
    }

    public String getPosterOriginalSizePath(ImagesConfiguration config){
        if(this.posterPath!=null && config != null && config.posterSizes != null && config.posterSizes.size()>0){
            return config.secureBaseUrl + config.posterSizes.get(config.posterSizes.size()-1) + this.posterPath;
        }
        return "";
    }

    private List<Genre> getGenres(List<Genre> genres){
        List<Genre> list = new ArrayList<>();
        if(this.genreIds != null && genres != null){
            for(long id : this.genreIds){
                for(Genre genre : genres){
                    if(genre.id == id){
                        list.add(genre);
                        break;
                    }
                }
            }
        }
        return list;
    }

    public String getGenreDescriptions(List<Genre> genres){
        String descriptions = "";
        List<Genre> list = getGenres(genres);
        for(Genre genre : list){
            descriptions = descriptions.equals("")? genre.name : descriptions + "-" + genre.name;
        }
        return descriptions;
    }

    public String getYear(){
        try {
            if(this.releaseDate != null && !this.releaseDate.equals("")){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = dateFormat.parse(this.releaseDate);
                SimpleDateFormat stringFormat = new SimpleDateFormat("yyyy");
                return stringFormat.format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
