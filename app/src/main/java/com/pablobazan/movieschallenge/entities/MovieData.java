package com.pablobazan.movieschallenge.entities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

public class MovieData implements Serializable {

    private static final String MOVIE_PREFERENCES = "movie_preferences";
    private static final String MOVIEINFO_DATA = "movieinfo_data";

    public List<Movie> topRatedMovies;
    public List<Movie> upcomingMovies;
    public List<Movie> popularMovies;
    public ImagesConfiguration config;
    public List<Genre> genreList;

    public MovieData(List<Movie> topRatedMovies, List<Movie> upcomingMovies, List<Movie> popularMovies, ImagesConfiguration config, List<Genre> genres){
        this.topRatedMovies = topRatedMovies;
        this.upcomingMovies = upcomingMovies;
        this.popularMovies = popularMovies;
        this.config = config;
        this.genreList = genres;
    }

    private String serialize() {
        return new Gson().toJson(this);
    }

    public void saveMovieInfo(Context context){
        try{
            SharedPreferences prefs = context.getSharedPreferences(MOVIE_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(MOVIEINFO_DATA, serialize());
            edit.commit();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private static MovieData deserialize(String s) {
        return new Gson().fromJson(s, MovieData.class);
    }

    public static MovieData getMovieInfo(Context context){
        try{
            SharedPreferences prefs = context.getSharedPreferences(MOVIE_PREFERENCES, Context.MODE_PRIVATE);
            String data = prefs.getString(MOVIEINFO_DATA, "");
            return deserialize(data);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
