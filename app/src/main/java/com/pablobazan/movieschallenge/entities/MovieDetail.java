package com.pablobazan.movieschallenge.entities;

import java.io.Serializable;
import java.util.List;

public class MovieDetail implements Serializable {
    public Movie movie;
    public ImagesConfiguration config;
    public List<Genre> genres;

    public MovieDetail(Movie movie, ImagesConfiguration config, List<Genre> genres){
        this.movie = movie;
        this.config = config;
        this.genres = genres;
    }
}
