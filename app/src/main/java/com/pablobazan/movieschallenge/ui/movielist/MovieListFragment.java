package com.pablobazan.movieschallenge.ui.movielist;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.snackbar.Snackbar;
import com.pablobazan.movieschallenge.R;
import com.pablobazan.movieschallenge.adapters.MovieAdapter;
import com.pablobazan.movieschallenge.databinding.FragmentMovieListBinding;
import com.pablobazan.movieschallenge.entities.Genre;
import com.pablobazan.movieschallenge.entities.ImagesConfiguration;
import com.pablobazan.movieschallenge.entities.Movie;
import com.pablobazan.movieschallenge.entities.MovieDetail;
import com.pablobazan.movieschallenge.interfaces.OnMovieClickedListener;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MovieListFragment extends Fragment {

    private FragmentMovieListBinding binding;
    private MovieListViewModel movieListViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        movieListViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);

        binding = FragmentMovieListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        movieListViewModel.restoreMovieData(getContext());
        ImagesConfiguration config = movieListViewModel.getConfig().getValue();
        List<Genre> genres = movieListViewModel.getGenreList().getValue();
        List<Movie> topRatedMovies = movieListViewModel.getTopRatedMovies().getValue();
        List<Movie> upcomingMovies = movieListViewModel.getUpcomingMovies().getValue();
        List<Movie> recommendedMovies = movieListViewModel.getPopularMovies().getValue();
        movieListViewModel.getConfig().observe(getViewLifecycleOwner(), configuration -> setConfig(configuration));
        movieListViewModel.getGenreList().observe(getViewLifecycleOwner(), items -> setGenres(items));
        movieListViewModel.getTopRatedMovies().observe(getViewLifecycleOwner(), items -> submitListToAdapter(binding.toprated,items));
        movieListViewModel.getUpcomingMovies().observe(getViewLifecycleOwner(), items -> submitListToAdapter(binding.upcoming,items));
        movieListViewModel.getPopularMovies().observe(getViewLifecycleOwner(), items -> applyFilters(binding.popular,items));
        movieListViewModel.getErrorMessage().observe(getViewLifecycleOwner(), error -> showError(error));
        binding.languageFilter.setOnClickListener(view -> movieListViewModel.toggleLanguageFilter());
        String currentLanguage = Locale.getDefault().getDisplayLanguage();
        binding.languageFilter.setText(getResources().getString(R.string.movie_list_language_button, currentLanguage));
        movieListViewModel.getLanguageFilter().observe(getViewLifecycleOwner(), language -> toggleChip(binding.languageFilter, language));
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        binding.yearFilter.setText(getResources().getString(R.string.movie_list_year_button, currentYear));
        binding.yearFilter.setOnClickListener(view -> movieListViewModel.toggleYearFilter());
        movieListViewModel.getYearFilter().observe(getViewLifecycleOwner(), year -> toggleChip(binding.yearFilter, year));

        initListView(binding.toprated, topRatedMovies, genres, config, movie -> navigateToDetailsPage(movie),0);
        initListView(binding.upcoming, upcomingMovies, genres, config, movie -> navigateToDetailsPage(movie),0);
        initListView(binding.popular, recommendedMovies, genres, config, movie -> navigateToDetailsPage(movie),2);

        binding.movieListSwipeRefresh.setOnRefreshListener(() -> movieListViewModel.getData(getContext()));

        movieListViewModel.getData(getContext());
        return root;
    }

    private void toggleChip(Chip view, String value){
        if (value == null || value.equals("")) {
            view.setChipBackgroundColorResource(R.color.black);
            view.setTextColor(Color.WHITE);
        }
        else{
            view.setChipBackgroundColorResource(R.color.white);
            view.setTextColor(Color.BLACK);
        }
        applyFilters(binding.popular, movieListViewModel.getPopularMovies().getValue());
    }

    private void setGenres(List<Genre> items){
        MovieAdapter popularAdapter = ((MovieAdapter)binding.popular.getAdapter());
        if(popularAdapter != null)
            popularAdapter.setGenres(items);
        MovieAdapter topratedAdapter = ((MovieAdapter)binding.toprated.getAdapter());
        if(topratedAdapter != null)
            topratedAdapter.setGenres(items);
        MovieAdapter upcomingAdapter = ((MovieAdapter)binding.upcoming.getAdapter());
        if(upcomingAdapter != null)
            upcomingAdapter.setGenres(items);
    }

    private void setConfig(ImagesConfiguration config){
        MovieAdapter popularAdapter = ((MovieAdapter)binding.popular.getAdapter());
        if(popularAdapter != null)
            popularAdapter.setConfig(config);
        MovieAdapter topratedAdapter = ((MovieAdapter)binding.toprated.getAdapter());
        if(topratedAdapter != null)
            topratedAdapter.setConfig(config);
        MovieAdapter upcomingAdapter = ((MovieAdapter)binding.upcoming.getAdapter());
        if(upcomingAdapter != null)
            upcomingAdapter.setConfig(config);
    }

    private void initListView(RecyclerView view, List<Movie> movies, List<Genre> genres, ImagesConfiguration config, OnMovieClickedListener listener, int numberOfColumns){
        MovieAdapter adapter = MovieAdapter.createMovieAdapter(movies, genres, config, listener);
        if(numberOfColumns > 1)
            view.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
        view.setAdapter(adapter);
    }

    public void navigateToDetailsPage(Movie movie){
        Bundle bundle = new Bundle();
        bundle.putSerializable("movieDetail", new MovieDetail(movie, movieListViewModel.getConfig().getValue(), movieListViewModel.getGenreList().getValue()));
        NavHostFragment.findNavController(getParentFragment()).navigate(R.id.action_nav_movie_list_to_nav_movie_detail, bundle);
    }

    private Snackbar createSnackBar(String message, int duration, int color) {
        View layout;
        Snackbar snackbar = Snackbar
                .make(getActivity().findViewById(android.R.id.content), message, duration);
        layout = snackbar.getView();
        layout.setBackgroundColor(color);
        TextView tv = layout.findViewById(com.google.android.material.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        else{
            tv.setGravity(Gravity.CENTER);
        }
        return snackbar;
    }

    private void showError(String error){
        if(error != null && !error.equals("")){
            createSnackBar(error, Snackbar.LENGTH_LONG, Color.RED).show();
            movieListViewModel.clearErrorMessage();
            binding.movieListSwipeRefresh.setRefreshing(false);
        }
    }

    private void applyFilters(RecyclerView listView, List<Movie> items){
        List<Movie> list = movieListViewModel.applyFiltersToMovieList((items));
        submitListToAdapter(listView,list);
    }

    private void submitListToAdapter(RecyclerView listView, List<Movie> items){
        ListAdapter adapter = (ListAdapter)listView.getAdapter();
        if(adapter != null)
            ((ListAdapter)listView.getAdapter()).submitList(items);
        binding.movieListSwipeRefresh.setRefreshing(false);
    }

    @Override
    public void onDestroyView() {
        movieListViewModel.saveData(getContext());
        super.onDestroyView();
        binding = null;
    }
}