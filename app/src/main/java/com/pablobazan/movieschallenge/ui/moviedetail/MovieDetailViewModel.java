package com.pablobazan.movieschallenge.ui.moviedetail;

import com.pablobazan.movieschallenge.entities.MovieDetail;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MovieDetailViewModel extends ViewModel {

    private final static String SEARCH_TRAILER = " movie trailer";

    private final MutableLiveData<String> title;
    private final MutableLiveData<String> genre;
    private final MutableLiveData<String> year;
    private final MutableLiveData<String> language;
    private final MutableLiveData<String> stars;
    private final MutableLiveData<String> plot;
    private final MutableLiveData<String> trailerUrl;
    private final MutableLiveData<String> posterUrl;

    public MovieDetailViewModel() {
        title = new MutableLiveData<>();
        genre = new MutableLiveData<>();
        year = new MutableLiveData<>();
        language = new MutableLiveData<>();
        stars = new MutableLiveData<>();
        plot = new MutableLiveData<>();
        trailerUrl = new MutableLiveData<>();
        posterUrl = new MutableLiveData<>();
    }

    public LiveData<String> getTitle() {
        return title;
    }
    public LiveData<String> getGenre() {
        return genre;
    }
    public LiveData<String> getYear() {
        return year;
    }
    public LiveData<String> getLanguage() {
        return language;
    }
    public LiveData<String> getStars() {
        return stars;
    }
    public LiveData<String> getPlot() {
        return plot;
    }
    public LiveData<String> getTrailerUrl() {
        return trailerUrl;
    }
    public LiveData<String> getPosterUrl() {
        return posterUrl;
    }

    public void setMovieDetail(MovieDetail movieDetail){
        if(movieDetail != null && movieDetail.movie != null){
            title.postValue(movieDetail.movie.title);
            if(movieDetail.genres != null)
                genre.postValue(movieDetail.movie.getGenreDescriptions(movieDetail.genres));
            year.postValue(movieDetail.movie.getYear());
            language.postValue(movieDetail.movie.originalLanguage);
            stars.postValue(String.valueOf(movieDetail.movie.voteAverage));
            plot.postValue(movieDetail.movie.overview);
            trailerUrl.postValue(movieDetail.movie.title + SEARCH_TRAILER);
            if(movieDetail.config != null)
                posterUrl.postValue(movieDetail.movie.getPosterOriginalSizePath(movieDetail.config));
        }
    }
}