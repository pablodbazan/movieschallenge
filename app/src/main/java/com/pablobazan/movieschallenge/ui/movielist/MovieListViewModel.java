package com.pablobazan.movieschallenge.ui.movielist;

import android.content.Context;

import com.pablobazan.movieschallenge.entities.ImagesConfiguration;
import com.pablobazan.movieschallenge.entities.Movie;
import com.pablobazan.movieschallenge.entities.Genre;
import com.pablobazan.movieschallenge.entities.MovieData;
import com.pablobazan.movieschallenge.interfaces.OnConfigurationReceivedListener;
import com.pablobazan.movieschallenge.interfaces.OnGenreListReceived;
import com.pablobazan.movieschallenge.services.MovieService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MovieListViewModel extends ViewModel {

    private final MutableLiveData<List<Movie>> topRatedMovies;
    private final MutableLiveData<List<Movie>> upcomingMovies;
    private final MutableLiveData<List<Movie>> popularMovies;
    private final MutableLiveData<ImagesConfiguration> config;
    private final MutableLiveData<List<Genre>> genreList;
    private final MutableLiveData<String> errorMessage;
    private final MutableLiveData<String> languageFilter;
    private final MutableLiveData<String> yearFilter;

    public MovieListViewModel() {
        topRatedMovies = new MutableLiveData<>();
        popularMovies = new MutableLiveData<>();
        upcomingMovies = new MutableLiveData<>();
        config = new MutableLiveData<>();
        genreList = new MutableLiveData<>();
        errorMessage = new MutableLiveData<>();
        languageFilter = new MutableLiveData<>();
        yearFilter = new MutableLiveData<>();
    }
    public LiveData<List<Movie>> getUpcomingMovies() {
        return topRatedMovies;
    }
    public LiveData<List<Movie>> getTopRatedMovies() {
        return upcomingMovies;
    }
    public LiveData<List<Movie>> getPopularMovies() {
        return popularMovies;
    }
    public LiveData<ImagesConfiguration> getConfig() {
        return config;
    }
    public LiveData<List<Genre>> getGenreList() {
        return genreList;
    }
    public LiveData<String> getErrorMessage() { return errorMessage; }
    public void clearErrorMessage() { errorMessage.setValue(""); }
    public LiveData<String> getLanguageFilter() {
        return languageFilter;
    }
    public LiveData<String> getYearFilter() {
        return yearFilter;
    }

    public void toggleLanguageFilter() {
        String currentValue = languageFilter.getValue();
        String language = Locale.getDefault().getLanguage();
        if(currentValue == null || currentValue.equals(""))
            languageFilter.postValue(language);
        else
            languageFilter.postValue("");
    }

    public void toggleYearFilter() {
        String currentValue = yearFilter.getValue();
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        if(currentValue == null || currentValue.equals(""))
            yearFilter.postValue(year);
        else
            yearFilter.postValue("");
    }

    public void getData(Context context){
        getConfigurationData(context, configuration -> {
            config.postValue(configuration);
            getGenreData(context, genres -> {
                genreList.postValue(genres);
                getTopRatedData(context,1);
                getUpcomingData(context,1);
                getPopularData(context,1);
            });
        });
    }

    public void getConfigurationData(Context context, OnConfigurationReceivedListener listener){
        MovieService movieService = new MovieService();
        movieService.getConfiguration(context, listener, error -> errorMessage.postValue(error));
    }

    public void getGenreData(Context context, OnGenreListReceived listener){
        MovieService movieService = new MovieService();
        movieService.getGenreList(context, listener, error -> errorMessage.postValue(error));
    }

    public void getTopRatedData(Context context,int page){
        MovieService movieService = new MovieService();
        movieService.getTopRatedMovies(context, page, movieList -> topRatedMovies.postValue(movieList), error -> errorMessage.postValue(error));
    }

    public void getUpcomingData(Context context,int page){
        MovieService movieService = new MovieService();
        movieService.getUpcomingMovies(context, page, movieList -> upcomingMovies.postValue(movieList), error -> errorMessage.postValue(error));
    }

    public void getPopularData(Context context,int page){
        MovieService movieService = new MovieService();
        movieService.getPopularMovies(context, page, movieList -> popularMovies.postValue(movieList), error -> errorMessage.postValue(error));
    }

    public List<Movie> applyFiltersToMovieList(List<Movie> movies){
        String language = languageFilter.getValue();
        String year = yearFilter.getValue();
        List<Movie> list = new ArrayList<>();
        for(Movie movie : movies){
            if((language == null || language.equals("") || movie.originalLanguage.startsWith(language)) && (year == null || year.equals("") || movie.getYear().equals(year))){
                list.add(movie);
            }
        }
        return list;
    }

    public void restoreMovieData(Context context){
        MovieData data = MovieData.getMovieInfo(context);
        if(data != null){
            if(data.config != null)
                config.postValue(data.config);
            if(data.genreList != null)
                genreList.postValue(data.genreList);
            if(data.topRatedMovies != null)
                topRatedMovies.postValue(data.topRatedMovies);
            if(data.upcomingMovies != null)
                upcomingMovies.postValue(data.upcomingMovies);
            if(data.popularMovies != null)
                popularMovies.postValue(data.popularMovies);
        }
    }

    public void saveData(Context context){
        MovieData data = new MovieData(topRatedMovies.getValue(), upcomingMovies.getValue(), popularMovies.getValue(), config.getValue(), genreList.getValue());
        data.saveMovieInfo(context);
    }
}