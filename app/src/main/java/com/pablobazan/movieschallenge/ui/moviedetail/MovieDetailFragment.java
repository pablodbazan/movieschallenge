package com.pablobazan.movieschallenge.ui.moviedetail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.pablobazan.movieschallenge.databinding.FragmentMovieDetailBinding;
import com.pablobazan.movieschallenge.entities.MovieDetail;
import com.squareup.picasso.Picasso;

public class MovieDetailFragment extends Fragment {

    private FragmentMovieDetailBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MovieDetailViewModel movieDetailViewModel =
                new ViewModelProvider(this).get(MovieDetailViewModel.class);

        binding = FragmentMovieDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        if(getArguments()!=null){
            MovieDetail movieDetail = (MovieDetail) getArguments().getSerializable("movieDetail");
            movieDetailViewModel.setMovieDetail(movieDetail);
        }
        movieDetailViewModel.getTitle().observe(getViewLifecycleOwner(), title -> binding.title.setText(title));
        movieDetailViewModel.getGenre().observe(getViewLifecycleOwner(), genre -> binding.genre.setText(genre));
        movieDetailViewModel.getYear().observe(getViewLifecycleOwner(), year -> binding.year.setText(year));
        movieDetailViewModel.getLanguage().observe(getViewLifecycleOwner(), language -> binding.language.setText(language));
        movieDetailViewModel.getStars().observe(getViewLifecycleOwner(), stars -> binding.stars.setText(stars));
        movieDetailViewModel.getPlot().observe(getViewLifecycleOwner(), plot -> binding.plot.setText(plot));
        movieDetailViewModel.getTrailerUrl().observe(getViewLifecycleOwner(), trailerUrl -> binding.btnTrailer.setOnClickListener(view -> launchTrailer(trailerUrl)));
        movieDetailViewModel.getPosterUrl().observe(getViewLifecycleOwner(), posterUrl -> Picasso.get().load(posterUrl).into(binding.imgDetail));
        return root;
    }

    private void launchTrailer(String trailerUrl){
        String url = "https://www.google.com/search?q="+trailerUrl;
        url = url.replace(" ","+");
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}