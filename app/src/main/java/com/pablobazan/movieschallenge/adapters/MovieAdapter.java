package com.pablobazan.movieschallenge.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.pablobazan.movieschallenge.R;
import com.pablobazan.movieschallenge.entities.Genre;
import com.pablobazan.movieschallenge.entities.ImagesConfiguration;
import com.pablobazan.movieschallenge.entities.Movie;
import com.pablobazan.movieschallenge.interfaces.OnMovieClickedListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class MovieAdapter extends ListAdapter<Movie, MovieAdapter.ViewHolder> {
    private OnMovieClickedListener listener;
    private List<Genre> genres;
    private ImagesConfiguration config;
    public static MovieAdapter createMovieAdapter(List<Movie> items, List<Genre> genres, ImagesConfiguration config, OnMovieClickedListener listener){
        MovieAdapter adapter = new MovieAdapter();
        adapter.setListener(listener);
        adapter.setGenres(genres);
        adapter.setConfig(config);
        adapter.submitList(items);
        return adapter;
    }

    public void setListener(OnMovieClickedListener listener) {
        this.listener = listener;
    }

    public void setGenres(List<Genre> genres){
        this.genres = genres;
    }

    public void setConfig(ImagesConfiguration config){
        this.config = config;
    }

    protected MovieAdapter() {
        super(new MovieDiffCallback());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_movie, parent, false);
        return new MovieAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie movie = getItem(position);
        holder.image.setContentDescription(movie.title);
        Uri uri = Uri.parse(movie.getPosterSmallSizePath(config));
        Picasso.get().load(uri).into(holder.image);
        if(listener != null){
            holder.item.setOnClickListener(view -> listener.OnMovieClicked(movie));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout item;
        public ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.movie_item);
            image = itemView.findViewById(R.id.movie_image);
        }
    }
}

class MovieDiffCallback extends DiffUtil.ItemCallback<Movie>{

    @Override
    public boolean areItemsTheSame(@NonNull Movie oldItem, @NonNull Movie newItem) {
        return oldItem.id == newItem.id;
    }

    @Override
    public boolean areContentsTheSame(@NonNull Movie oldItem, @NonNull Movie newItem) {
        return     oldItem.id == newItem.id
                && stringsAreEqual(oldItem.title,newItem.title)
                && stringsAreEqual(oldItem.posterPath,newItem.posterPath);
    }

    private boolean stringsAreEqual(String value1, String value2){
        if(value1 == null && value2 == null){
            return true;
        }
        else if(value1 != null && value2!= null){
            return value1.equals(value2);
        }
        else{
            return false;
        }
    }
}
